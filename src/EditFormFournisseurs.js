import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'

export default class EditFormFournisseurs extends React.Component{
    state = {
        fournisseur: {},
        loading: false,
        nom:'',
        telephone:'',
        ville:'',
        id:'',
    }

    componentDidMount() {
        this.setState({id:  this.props.match.params.id});
        axios.get('http://127.0.0.1:8000/api/fournisseurs/'+this.props.match.params.id)
            .then(res => {
                    this.setState({nom: res.data.nom})
                    this.setState({ville: res.data.ville})
                    this.setState({telephone: res.data.telephone})
                    console.log(this.state.fournisseur)
                }

            )
    }

    submitHandler = e => {
        e.preventDefault()
        axios.put('http://127.0.0.1:8000/api/fournisseurs/'+this.state.id, this.state).then(
            response => {
                console.log(response)
            }
        ).catch(err => {
            console.log(err)
        })
    }
    changeHandler = e => {
        this.setState({ [e.target.name]: e.target.value})
    }

    render(){
        const id = this.props.match.params.id;
        const { nom, telephone,ville} = this.state;
        return(
            <>
                <h1>Edition du fournisseur</h1>
                <form onSubmit={this.submitHandler}>
                    <div>
                        <label htmlFor="name">Nom : </label>
                        <input type="text" name="nom" value={this.state.nom} onChange={this.changeHandler} />
                    </div>
                    <div>
                        <label htmlFor="telephone">Téléphone : </label>
                        <input type="number" name="telephone" value={this.state.telephone} onChange={this.changeHandler}/>
                    </div>
                    <div>
                        <label htmlFor="ville">Ville : </label>
                        <input type="text" name="ville" value={this.state.ville} onChange={this.changeHandler}/>
                    </div>
                    <div>
                        <button type="submit">envoyer</button>
                    </div>
                </form>

            </>
        )
    }
}

