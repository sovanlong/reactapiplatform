import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'
import {Link, useParams} from "react-router-dom";

export default class Article extends React.Component{
    state = {
        article: [],

    }
    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get('http://127.0.0.1:8000/api/articles/'+id)
            .then(res => {
                    this.setState({article: res.data})
                console.log(this.state.article)
                }

            )

    }

    render(){
        return(
            <>
                <h1>Article: {this.state.article.description}   </h1>
                <li>prix: {this.state.article.prix}</li>
                <li>quantité: {this.state.article.quantite}</li>
                <li>référence: {this.state.article.reference}
                </li>



            </>
        )
    }
}

