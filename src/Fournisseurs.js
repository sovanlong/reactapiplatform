import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'
import {Link} from "react-router-dom";

export default class Fournisseurs extends React.Component{
    state = {
        fournisseurs: [],
        loading: false
    }
    componentDidMount() {
        axios.get('http://127.0.0.1:8000/api/fournisseurs?page=1')
            .then(res => {
                    this.setState({fournisseurs: res.data['hydra:member']})
                }

            )
    }
    handleDelete(id){
        axios.delete('http://127.0.0.1:8000/api/fournisseurs/'+id)
            .then(res => {
                    this.componentDidMount()
                }

            )
    }


    render(){
        return(
            <>
                <h1>Liste des fournisseurs  </h1>
                {this.state.fournisseurs.map(item => {
                    return(
                        <>
                            <li key={item}> {item.nom} <button onClick={() => this.handleDelete(item.id)}>supprimer</button> <button> <Link to={"/fournisseurs/edit/"+item.id}>Editer</Link></button>  </li>

                        </>

                    )
                })}
            </>
        )
    }
}

