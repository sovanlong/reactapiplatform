import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'
import {Link} from "react-router-dom";

export default class Articles extends React.Component{
    state = {
        articles: [],
        loading: false
    }
    componentDidMount() {
        axios.get('http://127.0.0.1:8000/api/articles?page=1')
            .then(res => {
                    this.setState({articles: res.data['hydra:member']})
                }

            )
    }

    render(){
        return(
            <>
                <h1>Liste des articles  </h1>
                {this.state.articles.map(item => {
                    return(
                        <><li key={item}> {item.description} </li>
                            <Link to={'article/' + item.id} >
                                {item.id}
                            </Link>
                        </>



                    )
                })}
            </>
        )
    }
}

