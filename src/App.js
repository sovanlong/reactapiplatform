import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'
import Fournisseurs from "./Fournisseurs";
import { Link } from 'react-router-dom'
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Articles from "./Articles";
import Article from "./Article";
import FormFournisseurs from "./FormFournisseurs";
import EditFormFournisseurs from "./EditFormFournisseurs";


export default function App() {
    return (
        <Router>
            <div>
                <nav>
                    <ul>
                        <li>
                            <Link to="/fournisseurs">Fournisseurs</Link>
                        </li>
                        <li>
                            <Link to="/articles">Articles</Link>
                        </li>
                        <li>
                            <Link to="/addFournisseurs">Ajouter un fournisseur</Link>
                        </li>
                    </ul>
                </nav>

            </div>


            <Switch>
                <Route exact path="/articles">
                    <Articles />
                </Route>

                <Route exact path="/article/:id" component={Article}>
                </Route>

                <Route exact path="/fournisseurs">
                    <Fournisseurs />
                </Route>

                <Route exact path="/fournisseurs/edit/:id" component={EditFormFournisseurs} />
                <Route path="/addFournisseurs">
                    <FormFournisseurs/>
                </Route>
            </Switch>
        </Router>



    );

}

