import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios'

export default class FormFournisseurs extends React.Component{
    state = {
        fournisseurs: [],
        loading: false,
        nom:'',
        telephone:'',
        ville:'',
    }
    componentDidMount() {
        axios.get('http://127.0.0.1:8000/api/fournisseurs?page=1')
            .then(res => {
                    this.setState({fournisseurs: res.data['hydra:member']})
                }

            )
    }

    submitHandler = e => {
        e.preventDefault()
        axios.post('http://127.0.0.1:8000/api/fournisseurs', this.state).then(
            response => {
                console.log(response)
            }
        ).catch(err => {
            console.log(err)
        })
    }
    changeHandler = e => {
        this.setState({ [e.target.name]: e.target.value})
    }

    render(){
        const { nom, telephone,ville} = this.state;
        return(
            <>
                <form onSubmit={this.submitHandler}>
                    <div>
                        <label htmlFor="name">Nom : </label>
                        <input type="text" name="nom" value={nom} onChange={this.changeHandler} />
                    </div>
                    <div>
                        <label htmlFor="telephone">Téléphone : </label>
                        <input type="number" name="telephone" value={telephone} onChange={this.changeHandler}/>
                    </div>
                    <div>
                        <label htmlFor="ville">Ville : </label>
                        <input type="text" name="ville" value={ville} onChange={this.changeHandler}/>
                    </div>
                    <div>
                        <button type="submit">envoyer</button>
                    </div>
                </form>

            </>
        )
    }
}

